open Batteries
module US = Safe_int.Unsafe_int
module GS = Safe_int.Gadt_int
module Log = Safe_int.Logic

let () = let a = US.Pos 5 and b = US.ZPos 1 in
  ignore (US.div a b);
  let a = GS.Pos 3 and b = GS.ZPos 1 in
  let apb = Log.eval_left (GS.add (Log.or_left a b)) in
  let apbzero = GS.add_left apb (ZPos 0) in
  print_endline (GS.show_safe_int apbzero);
  (*let forbidden = GS.div apbzero b in 
  print_endline (GS.show_safe_int forbidden);*)
  let allowed = GS.check_div apbzero (ZPos 5) in 
  print_endline (GS.show_safe_int (Option.get allowed));
