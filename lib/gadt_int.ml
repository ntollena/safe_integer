open Batteries
open Logic
type pos = P
type neg = N
type z = Z
type nz = NZ

type _ safe_int  =
  | Pos: int ->  nz safe_int
  | ZPos: int ->  z safe_int

let show_safe_int: type a. a safe_int -> string = function
  | Pos x -> Printf.sprintf "Pos %d" x
  | ZPos x -> Printf.sprintf "ZPos %d" x

let eval: type a. a safe_int -> int = function
  | Pos x -> x
  | ZPos x -> x

let add (type a b one_nz): (nz safe_int, z safe_int, a safe_int, b safe_int, one_nz) or_t 
  ->  (nz safe_int, z safe_int, one_nz) either = 
  function
    | Both (Pos x, Pos y, _, _) -> Left (Pos (x + y))
    | Or_left (Pos x, ZPos y, _, _) -> Left (Pos (x + y))
    | Or_right (ZPos x, Pos y, _, _) -> Left (Pos (x + y))
    | Neither (ZPos x, ZPos y, _, _) -> Right (ZPos (x + y))
    | _ -> .


let add_both : nz safe_int -> nz safe_int ->  nz safe_int = fun i1 i2 ->
  match i1, i2 with
  | Pos x, Pos y -> add (or_both (Pos x) (Pos y))
  |> eval_left

let add_left: nz safe_int -> z safe_int -> nz safe_int = fun i1 i2 ->
  match i1, i2 with
  | Pos x, ZPos y -> add (or_left (Pos x) (ZPos y))
  |> eval_left

let add_right: z safe_int -> nz safe_int -> nz safe_int = fun i1 i2 ->
  match i1, i2 with
  | ZPos x, Pos y -> add (or_right (ZPos x) (Pos y))
  |> eval_left

let add_neither: z safe_int -> z safe_int -> z safe_int = 
  fun i1 i2 -> add (or_neither i1 i2) |> eval_right

let add_either : e1 e2. (nz safe_int, z safe_int, e1) either 
-> (nz safe_int, z safe_int, e2) either -> (nz safe_int, z safe_int, (e1, e2, _) bool_t) either =
  fun ei1 ei2 -> match ei1, ei2 with
    | Left v1, Left v2 -> add (or_both v1 v2)
    | Left v1, Right v2 -> add (or_left v1 v2)
    | Right v1, Left v2 -> add (or_right v1 v2)
    | Right v1, Right v2 -> add (or_neither v1 v2)

let mulnn (i1: nz safe_int) (i2:nz safe_int): nz safe_int = 
  match i1, i2 with
  | Pos x, Pos y -> Pos (x * y)

let mul_any (type a b)  (i1: a safe_int) (i2: b safe_int)  =
  ZPos (eval i1 * (eval i2))

let div: type a. a safe_int -> nz safe_int -> z safe_int =
  fun i1 i2 -> match i2 with
  | Pos d -> ZPos (eval i1 / d)
  
let check_div:type a. a safe_int -> z safe_int -> (z safe_int) option =
  fun i1 i2 -> match i2 with
  | ZPos 0 -> None
  | ZPos d -> Some (ZPos (eval i1 / d))
