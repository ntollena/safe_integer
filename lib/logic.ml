type true_ = T
type false_ = F

type (_, _) eql = Refl : ('a, 'a) eql

type (_,_,_) bool_t =
  | C : 'a -> ('a, 'a, 'a) bool_t 
  | Or_TT: ('a, 'b, true_) bool_t * ('c,'d,_) bool_t ->  ('a * 'b, 'c * 'd, true_) bool_t
  | Or_TF: ('a, 'b, false_) bool_t * ('c, 'd, true_) bool_t  ->  ('a * 'b, 'c * 'd, true_) bool_t
  | Or_FF: ('a, 'b, false_) bool_t * ('c, 'd, false_) bool_t ->  ('a * 'b, 'c * 'd, false_) bool_t


(* proof that I can obtain 'a from either 'c or 'd *)
type ('a,  'b, 'c, 'd, _) or_t =
  | Both: 'c * 'd * ('a, 'c) eql * ('a, 'd) eql  -> ('a, 'b, 'c, 'd, (_,_,true_) bool_t) or_t
  | Or_left: 'c * 'd * ('a, 'c) eql * ('b, 'd) eql -> ('a, 'b, 'c, 'd, (_,_,true_) bool_t) or_t
  | Or_right: 'c * 'd * ('b, 'c) eql * ('a, 'd) eql -> ('a, 'b, 'c, 'd, (_,_,true_) bool_t) or_t 
  | Neither: 'c * 'd * ('b, 'c) eql * ('b, 'd) eql -> ('a, 'b, 'c, 'd, (_,_,false_) bool_t) or_t

let or_both i1 i2 =
  Both (i1, i2, Refl, Refl)

let or_left i1 i2 =
  Or_left (i1, i2, Refl, Refl)

let or_right i1 i2 =
  Or_right (i1, i2, Refl, Refl)

let or_neither i1 i2 =
  Neither (i1, i2, Refl, Refl)

(* proof that I can obtain 'a from either 'c or 'd *)
type ('a,  'b, 'c, 'd, _) and_t =
  | And: 'c * 'd * ('a, 'c) eql * ('a, 'd) eql  -> ('a, 'b, 'c, 'd, true_) and_t
  | Nand: 'c * 'd  -> ('a, 'b, 'c, 'd, false_) and_t

let and_b i1 i2 =
  And (i1, i2, Refl, Refl)

let nand i1 i2 =
  Nand (i1, i2)

type ('a, 'b, _) either =
  | Left: 'a -> ('a, 'b, (_, _, true_) bool_t) either
  | Right: 'b -> ('a, 'b, (_, _, false_) bool_t) either

let left a = Left a
let right b = Right b

let eval_left:  ('a, _, (_,_,true_) bool_t) either -> 'a =
  function Left a -> a

let eval_right:  (_, 'b, (_,_,false_) bool_t) either -> 'b =
  function Right b -> b
