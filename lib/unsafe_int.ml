type safe_int =
| Pos of int
| ZPos of int
| Neg of int
| ZNeg of int
| NonZero of int
| Any of int

let get = function
| Pos x | ZPos x -> x
| Neg x | ZNeg x -> -x
| NonZero x | Any x -> x

let add i1 i2 = match i1, i2 with
| Pos x, Pos y| Pos x, ZPos y| ZPos x, Pos y-> Pos (x+y)
| ZPos x, ZPos y -> ZPos (x+y)
| Neg x,Neg y | ZNeg x, Neg y | Neg x, ZNeg y -> ZNeg (x + y)
| _,_ -> Any(get i1 + (get i2))

let mul i1 i2= match i1, i2 with
| Pos x, Pos y | Neg x, Neg y -> Pos (x*y)
| Pos x, Neg y | Neg x, Pos y -> Neg (x*y)
| Pos x, NonZero y | NonZero x, Pos y
| NonZero x, NonZero y -> NonZero (x * y)
| Neg x, NonZero y | NonZero x, Neg y -> NonZero (-x * y)
| _, _ -> Any (get i1 * (get i2))

let div i1 i2 = match i2 with
| NonZero _ | Pos _ | Neg _ -> Any (get i1 / (get i2))
| _ -> (match get i2 with
  | 0 -> failwith "Division by zero" (*Unsafe!!!!!*)
  | n -> Any(get i1 / n)
)
